All thanks to Author Anubhav Singh for an excellent article on reinforcement learning 
Article can be reffered : https://www.datacamp.com/community/tutorials/introduction-reinforcement-learning#what-is-rl

Problem :  assume you're at a casino and in a section with some slot machines. Let's say you're at a section with 10 slot machines in a row and 
it says "Play for free! Max payout is 10 dollars" Each slot machine is guaranteed to give you a reward between 0 and 10 dollars. 
Each slot machine has a different average payout, and you have to figure out which one gives the most average reward so that you can maximize 
your reward in the shortest time possible.

Solution can be found in  the code . 