﻿// MultipleBanditArmProb.cpp : Defines the entry point for the application.
//

#include "MultipleBanditProb.h"
#include <vector>
#include <numeric>
#include <random>

/* Uses Episilon greedy algorithm for solving the multiple bandit Problem*/

std::default_random_engine probability;
std::uniform_int_distribution<int> distribution(0, 100);
#define NUM_EPISODES 500 
#define epsilon 0.1  // The lower the epsilon more greedy choices agent towards the best arm choice will make and higher the epsilon more random are the choices
#define Arms 10
std::uniform_int_distribution<int> armChoice(1, Arms);
std::uniform_int_distribution<int> EpisodeRand(0, 100);

int calculateRwd(float prob) {
	int reward = 0;
	std::vector<int> v(10);
	std::iota(std::begin(v), std::end(v), 0);
	for (int& i : v) {
		int anum = distribution(probability);
		double aprob = anum / (double)100;
		if (aprob < prob) {
			reward += 1;
		}
	}
	return reward;
}

int getBestArm(std::vector<std::pair<int,int>> av) {
	int bestArm = 0;
	double bestmeanReward = 0;
	for (int i = 1; i <= Arms; i++) {
		int sum = 0;
		int count = 0;
		for (auto& aVelem : av) {
			if (aVelem.first == i) {
				sum += aVelem.second;
				count += 1;
			}
		}
		double avg = sum / (double)count;
		if (bestmeanReward < avg) {
			bestArm = i;
			bestmeanReward = avg;
		}
	}
	return bestArm;
}

double CalculateMeanReward(std::vector<std::pair<int, int>> av) {
	int sum = 0;
	int count = 0;
	for (auto& avElem : av) {
		sum += avElem.second;
		count += 1;
	}
	return sum / (double)count;
}

int main()
{
	std::vector<std::pair<int, int>> actionValue;
	std::vector<double> prob(Arms);
	int arm = 1;
	for (auto& probindi : prob) {
		probindi = distribution(probability) / (double)100;
		std::cout << "Arm " << arm << " Probability of Winning " << probindi << std::endl;
	}
	std::vector<int> v(NUM_EPISODES);
	std::iota(std::begin(v), std::end(v), 0);	
	
	for (int& i : v) {
		double anum = EpisodeRand(probability)/(double)100;
		std::cout << "Playing Episode : " << i << std::endl;
		std::cout << "Aquired anum : " << anum << std::endl; 
		int arm_choice;
		int reward;
		if (anum > epsilon) {
			std::cout << "This episode selects based on previous known values!!" << std::endl;
			/*Selecting the best arm*/
			arm_choice = getBestArm(actionValue);
			/*Getting a reward for that arm*/
			reward = calculateRwd(arm_choice);
			/*Adding to actionValue memory array*/
			actionValue.push_back(std::make_pair(arm_choice, reward));
		}
		else {
			std::cout << "This episode selects based on random actions!!" << std::endl;
			/*Selecting a random arm*/
			arm_choice = armChoice(probability) - 1;
			/*Getting the reward for that choice*/
			reward = calculateRwd(prob[arm_choice]);
			/*Storing the av value*/
			actionValue.push_back(std::make_pair(arm_choice, reward));
		}
		std::cout << "Average Reward : " << CalculateMeanReward(actionValue) << std::endl;
		std::cout << "Arm Choice : " << arm_choice << "\nReward : " << reward << std::endl; 

	}

	//calculateRwd(0.6);
	std::cout << "Hello CMake." << std::endl;
	return 0;
}
